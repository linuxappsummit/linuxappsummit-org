---
layout: page
title: "Enforcing our Code of Conduct"
permalink: /enforcing-coc/
---


## Public Statement on CoC Enforcement

The Linux App Summit (LAS) wishes everyone to feel safe and free from harassment at our event. To ensure everyone's security, we have a Code of Conduct (CoC) that every attendee must follow while participating in our event:  
  
[https://linuxappsummit.org/coc/](https://linuxappsummit.org/coc)  
  
We understand that online discussions between community members can sometimes become heated and can lead to brusque, even rude, interactions.
Although we do not condone this kind of behavior, and, as long as the comments in question are not discriminatory and denigrating to any collective based on race, creed, gender, sexual orientation, or physical or mental divergence; LAS organizers refrain from judging people by the things they may have said prior to the event.  
  
HOWEVER  
  
These kinds of comments and attitudes will not be tolerated during our event under any circumstances. We expect all attendees to respect each other and behave with decorum towards fellow community members.  
  
Any egregious violation of the CoC will have consequences and will result in the offender being removed from the conference.  
  
We want to reassure our attendees that any statements or comments made by any speaker before, during, or after LAS are the speaker's personal opinions and do not reflect the views or opinions of LAS, GNOME, or KDE. If you feel that someone has violated a condition of the CoC, please inform a LAS committee member immediately so that we may take direct action:  
  
[https://linuxappsummit.org/coc/report](https://linuxappsummit.org/coc/report)  

Linux App Summit is run by volunteers and enthusiasts - who want to build a vibrant community. We might not always get it right but we are learning. Please reach out to us if there are any concerns about this statement: [info@linuxappsummit.org](mailto:info@linuxappsummit.org)  
