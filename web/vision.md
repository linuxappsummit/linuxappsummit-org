---
layout: page
title: "About us"
permalink: /vision/
---

# Vision
Providing a meeting point for app creators to collaborate on offering the best Linux user experience.

# Mission
* Offer a space for the different system and app creators to meet and build together.
* Nurture a welcoming ecosystem which promotes diversity and participation.
* Establish a go-to event for creators developing applications for non-Linux platforms, and provide an environment to learn how to embrace Linux-based systems.
* Shed light on how to make products for the Linux platform sustainable; be it technically, economically, or otherwise.

# Strategy
* Stimulate collaboration within the ecosystem.
* Offer a coherent front to 3rd party application developers who want to make sure their software is available on Linux.
* Enable a business-friendly environment.
* Facilitate app creators to meet key contributors, projects, businesses, and organizations in the Linux app ecosystem.
* Reliably provide physical space for conversations to happen.
* Provide a space where people can discuss what they are working on and care about.
* Make sure different approaches to Linux application creation, distribution, and any concept in-between, are heard and can be accounted for.

# Chronology
### 2019:
* We organised a joint conference between GNOME and KDE.
* Most of those who could and wanted, managed to come and know us, explain their story.
* We exposed ourselves as a unified front to 3rd party providers as well as possible sponsors.

### 2020:
* We reiterate, we are here to stay
* We continue the conversation with the 3rd parties, we find ways to get them more involved.
* ~~We co-locate and manage to get our voice out more easily.~~ Virtual.

### 2021:
* We move to spring
* We build upon last year's work, we stay virtual

## 2022:
* LAS happens again
* The world is not ready for predictions just yet
