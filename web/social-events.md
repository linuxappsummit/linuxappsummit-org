---
layout: page
title: "Social Events"
permalink: /social-events/
---


We have planned a few social events, so please come and join us. Note that for the walking tour, you do need to register by April 20.

## Pre-registration Party - Thu April 28 @ 6-8pm

Meet us at the venue the evening before the conference starts to pick up your badge, and say hello! We will be in the bar area so come get your badge and say hello!

## Ubuntu 22.04 - Jammy Jellyfish - Release Party - Sat April 30 @ 7-9:30pm

Join Canonical and friends in celebrating Jammy Jellyfish (released April 21). There will be snacks, drinks, and a release cake! The location is somewhere at the venue (actual room TBD) and will lead up to the Social evening event (read more in the next section).

For those of you that can't join us in person, make sure to catch our virtual release party on [Friday @ 2:05pm](https://conf.linuxappsummit.org/event/4/contributions/87/)!

## Social evening - Sat April 30 @ 9:30-12am

On the last night of the conference, April 30, go and have dinner and then come back to the SmartLab space. We will have a a DJ performing from 9:30pm till midnight, and the venue's bar remains open even later. 

## Walking tour - Sun May 1 @ 9:30am

**Participation upon registration via the [signup form](https://conf.linuxappsummit.org/event/4/registrations/10/). Registration closes on 15 of April**.

Note that if we do not have enough signups (20 minimum), then we will not be able to book the tour, but you can still go and get a tour but it may cost a little more.

![Walking Tour pic 1](/assets/walking-tour-pic-1.jpeg)
![Walking Tour pic 2](/assets/walking-tour-pic-2.jpeg)

Photo Credits: visitrovereto.it

We will go up to the “Bosco della città” (Wood of the City), through a footpath
and back to the city by a secondary road.

During the walk we will have time for random chat and to enjoy togheter the
surrounding.

The walk in the park will be guided by a professional Accompagnatore di Media
Montagna (Mountain Leader).

During our walk we will stop to admire the landscape and our guide will provide
details about:

-   vineyards
-   agricultural terrace
-   the wood near the city with trees and animals
-   a panoramic view of Beseno Castle
-   a view of the Vallagarina Valley (with mount Stivo, Finonchio, Zugna)
-   an unfinished building from the sixties: “ex ANMIL”, a monument to the Italian
    bureaucracy and missing political accountability
-   sperimentarea (experiment-area), a park for school projects in the nature (now closed)
-   wineries along the way (but we do not stop by)
-   a panoramic view of the rock of Dinosaurs Tracks
-   a panoramic view of the Monumental Shrine “Sacrario Militare Castel Dante” that
    hosts the remains of more than 20.000 soldiers of the First World War

We will end our group tour near the Rovereto Castle where you can visit the
Italian War History Museum.

The usual duration for a visit to the Castle War Museum is one hour and half.

From the ending point it will take around 30 minutes to walk back to SmartLab,
or 20 minutes to walk to the train station.

The visit of the Castle War Museum is self-managed (audio guides available in
English).

In case of bad weather the tour may be canceled.

See also the links:

-   <https://www.comune.rovereto.tn.it/Percorsi/Ambiente/Bosco-della-Citta/Percorso-Natura>
-   <https://museodellaguerra.it/>


### Technical Summary

-   Difficulty: easy
-   Distance around 7 km
-   Duration 2:00/2:30 h
-   Ascent: 250 m
-   Descent: 250 m
-   Starting date: Sunday 1st May 2022
-   Starting time: 9:30 AM
-   Staring Point: SmartLab (conference venue)
-   Ending time: 11:30 AM /12:00 PM
-   Ending Point: Castle of Rovereto, Italian War History Museum


### Bring with you

You need to be wearing the right clothing and take the proper gear when you go
hiking in the hills. Hiking shoes are the best. Training shoes are acceptable.

You should have in a small rucksack/backpack:

-   water bottle (at least 0.5L)
-   snacks (e.g. mixed dried fruit or chocolate)
-   windproof jacket
-   fleece
-   spare t-shirt (in a plastic bag)
-   make sure to wear or at least bring a pair of long trousers
-   sun screen
-   sunglasses
-   optional binoculars
-   trekking poles (if generally used)


### Confirmation

The minimum number of persons is 20 the max is 25. The total cost per person
(walk + castle) is 20€.

Special arrangements can be done for who needs to leave before the visit to the
castle.

After the 15 april we will confirm the tour if there are enough persons
subscribed.

**Sign up to the tour by filling the [registration form](https://conf.linuxappsummit.org/event/4/registrations/10/). Registrations closes on 15 of April.**
