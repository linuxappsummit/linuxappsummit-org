---
layout: page
title: "Sponsors"
permalink: /sponsors/
style: sponsors-page
---

# Sponsor LAS 2025!
The Linux Application Summit is one of the most unique conferences in the open source world. It brings together a complete vertical stack of FOSS contributors and relevant corporate organizations to work together on building a market for applications for the Linux platform. 

If successful, we will bring about a new viable alternative to closed source ecosystems and will create a marketplace for Linux apps that will greatly benefit application developers, entrepreneurs, and software companies alike.

Your sponsorship will help us build this new market for Linux applications.

*For information about sponsorship opportunities, please take a look at our [sponsorship brochure](/assets/2025LAS-Brochure.pdf) and contact us at [sponsors@linuxappsummit.org](mailto:sponsors@linuxappsummit.org).*

# LAS Sponsors

The following are our current sponsors for 2025.

## SUPPORTER

### TUXEDO Computers

<img class="sponsorlogo" src="/assets/tuxedo.svg" alt="TUXEDO"/>

> TUXEDO Computers builds tailor-made hardware with Linux!
>
> For a wide variety of computers and notebooks - all of them individually
built and prepared - we are the place to go. From lightweight ultrabooks
up to full-fledged AI development stations TUXEDO covers every aspect of
modern Linux-based computing.
> In addition to that we provide customers with full service at no extra
cost: Self-programmed driver packages, tech support, fully automated
installation services and everything around our hardware offerings.
>
> [https://www.tuxedocomputers.com](https://www.tuxedocomputers.com)

### openSUSE

<img class="sponsorlogo" src="/assets/openSUSE.svg" alt="openSUSE"/>

> The openSUSE project is a worldwide effort that promotes the use of Linux everywhere.
>
> The openSUSE community develops and maintains a packaging and distribution infrastructure, which provides the foundation for the world’s most flexible and powerful Linux distribution. Our community works together in an open, transparent and friendly manner as part of the global Free and Open Source Software community.

> [https://opensuse.org](https://opensuse.org)
 
